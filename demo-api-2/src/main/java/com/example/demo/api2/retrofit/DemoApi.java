package com.example.demo.api2.retrofit;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface DemoApi {

    static DemoApi buildRetroFit(String baseUrl) {

        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build();
        return retrofit.create(DemoApi.class);
    }

    @GET("/internal/welcome")
    Call<Void> callDemoApiWelcome();
}
