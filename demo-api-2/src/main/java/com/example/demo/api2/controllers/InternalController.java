package com.example.demo.api2.controllers;

import com.example.demo.api2.retrofit.DemoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.Call;
import retrofit2.Response;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/internal")
public class InternalController {

    @Autowired
    private DemoApi demoApi;

    @Value("${external-api.demo-api.url}")
    private String demoApiUrl;

    @GetMapping(path = {"/call-demo-api"})
    public void callApi1() {
        System.out.println("internal call - " + LocalDateTime.now());
        Call<Void> call = demoApi.callDemoApiWelcome();
        try {
            Response<Void> response = call.execute();
            if (response.isSuccessful()) {
                System.out.println("Appel réussi ! Statut code : " + response.code());
            } else {
                System.out.println("Erreur : " + response.errorBody());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
