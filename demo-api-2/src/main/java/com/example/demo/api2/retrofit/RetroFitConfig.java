package com.example.demo.api2.retrofit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RetroFitConfig {

    @Value("${external-api.demo-api.url}")
    private String demoApiUrl;

    @Bean
    public DemoApi demoApi() {
        return DemoApi.buildRetroFit(demoApiUrl);
    }
}
