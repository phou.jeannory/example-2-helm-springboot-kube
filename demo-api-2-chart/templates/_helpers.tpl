{{/* Définition du modèle demo-api-2-chart.fullname */}}
{{- define "demo-api-2-chart.fullname" -}}
{{- printf "%s" .Release.Name -}}
{{- end -}}

{{/* Définition du modèle demo-api-2-chart.name */}}
{{- define "demo-api-2-chart.name" -}}
{{- printf "%s" .Chart.Name -}}
{{- end -}}

{{/* Définition du modèle external-api-demo-api-url */}}
{{- define "external-api-demo-api-url" -}}
{{ .Values.externalApi.demoApi.url }}
{{- end -}}

{{/* Définition du modèle server-port */}}
{{- define "server-port" -}}
{{ .Values.server.port }}
{{- end -}}